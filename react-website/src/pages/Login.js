import React from 'react';
import {Redirect} from 'react-router-dom';
import './style.css';
import {loginUser} from '../components/api/userApi';




    
     class Login extends React.Component {
         constructor(props) {
             super(props);
             this.state = {
                 redirect: false,
                 error: null
             }
             this.email = "";
             this.password = "";
         }
         
         onChangeText(type, text) {
             this[type] = text;
         }
         
         onSubmitForm = async () => {
             let data = {
                 email: this.email,
                 password: this.password
             }
             console.log(data);

             try {

                  const res = await loginUser(data)
                 
                  if(res.status === 200) {
                     window.localStorage.setItem('adventure-token', res.token);
                     this.setState({redirect:true})

                 } else if (res.status === 404) {
                    this.setState({error: res.msg})

                 } else if (res.status === 401) {
                     this.setState({error: res.msg})

                 } else {
                     this.setState({error: 'Un problème est survenu !'})
                 }
                 
             } catch (error) {
                
                this.setState({error: 'Un problème Serveur !'})

             }

            
              
            
         }
         
         render(){
 

  
    return (
      <div>
          {this.state.redirect && <Redirect to="/" />}
          {this.state.error !== null && <p style={{color: "red"}}>{this.state.error}</p>}
        <div className="base-container">
          <div className="head">Login</div>
          <div className="content">
            <form className="form"
                onSubmit={(e)=>{
                    e.preventDefault();
                    this.onSubmitForm();
                }
            }>
              
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input 
                    type="text" 
                    name="email" 
                    placeholder="email"
                    onChange={(e)=>{

                        this.onChangeText('email', e.currentTarget.value);
                    }}
                />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input 
                    type="password" 
                    name="password" 
                    placeholder="password" 
                    onChange={(e)=>{
                        this.onChangeText('password', e.currentTarget.value);
                    }}
                />
              </div>
              <div className="foot">
                <button type="submit" className="btn">
                  Login
                </button>
              </div>
            </form>
          </div>
          
        </div>
      </div>  
      );
    
    }
     }
export default Login
