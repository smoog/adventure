import axios from 'axios';
import {config} from '../../config';

// const token = window.localStorage.getItem('adventure');

const moduleSegment = "api/v1/user"

export const saveUser = async (credential)=>{
   
    const { data } = await axios.post(`${config.api_url}/${moduleSegment}/save`, credential)
    return data
           
}

export const loginUser = async (credential)=> {
    
        const { data } = await axios.post(`${config.api_url}/${moduleSegment}/login`, credential)
        return data
}


