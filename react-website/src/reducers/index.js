import { combineReducers } from "redux";
import UserReducer from './userReducer';

//Exchange for Reducers
const rootReducer = combineReducers({
    user: UserReducer,
    
})

export default rootReducer;