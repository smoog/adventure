const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
if(!process.env.HOST_DB) {
	var config = require('../config')
} else {
	var config = require('../config-exemple')
}
const secret = process.env.TOKEN_SECRET||config.token.secret;
const withAuth = require('../withAuth');


module.exports = (app, db)=>{
    const userModel = require('../models/UserModel')(db);
    
    app.post('/api/v1/user/save', async (req, res, next)=>{
	    let response = await userModel.saveOneUser(req);
	    console.log(response)
	    res.json(response)
	    
	})
	
    app.post('/api/v1/user/login', async (req, res, next)=>{
		let user = await userModel.getOneUserByMail(req.body.email)
		console.log(user);
		
		if (user.status === 404) {
			res.json(user)
		}
		
		let same = await bcrypt.compare(req.body.password, user.password);
		
		if(same) {
			let infos = {id: user.id, email: user.email}
			
			let token = jwt.sign(infos, secret);
			
			res.json({status: 200, token: token, msg: "utilisateur bien connecté"})
		} else {
			res.json({status: 401, msg: "utilisateur non autorisé"})
		}
		
    })
 
}
