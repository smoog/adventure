const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const secret = "adventure";
const withAuth = require('../withAuth');


module.exports = (app, db)=>{
    const userModel = require('../models/UserModel')(db);
    
    app.get('/api/v1/checkToken', withAuth, async (req, res, next)=>{
        let user = await userModel.getOneUserByMail(req.email) 
        
	    res.json({status: 200, msg: "token valide ", user: user})
	})
	
 
}