const bcrypt = require('bcrypt');

const saltRounds = 10;

module.exports =(_db)=> {
    db = _db
    return UserModel;
}

class UserModel {

    static async saveOneUser(req){
        let hash = await bcrypt.hash(req.body.password,saltRounds)

        return db.query('INSERT INTO users (userName, password, email) VALUES (?, ?, ? )', [req.body.userName, hash, req.body.email])
        .then((result)=>{
            console.log('REPONSE',result)
           return {status: 200, msg: "utilisateur bien enregistré !"}
        })
        .catch((err)=>{
            return {status: 500, msg: err}
        })
    }
    static async getOneUserByMail(email) {
        return db.query('SELECT * FROM users WHERE email = ?', [email])
                .then((user)=>{
                    if(user.length === 0) {
                        return {status: 404, msg: "le mail n'existe pas dans la base de donnée"}
                    }
                    return user[0]
                })
                .catch((err)=>{
                    return err
                })
    } 
    static async updateUser(req) {
        return db.query('UPDATE users SET firstName = ?,  WHERE id =?', [req.body.userName, req.body.id])
                .then((result)=>{
                   
                    return result;
                })
                .catch((err)=>{
                    return err
                })
    }
}
