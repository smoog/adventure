const express = require('express');
const app = express();
app.use(express.static(__dirname + '/public'));
app.set('views', './views'); 
app.set('view engine', 'ejs'); //permet l'affichage des templates ejs
const bodyParser = require('body-parser');
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));


/* Upload un fichier */


const fileUpload = require("express-fileupload");
app.use(fileUpload({
    createParentPath: true
}))
const cors = require('cors');
app.use(cors());
if(!process.env.HOST_DB) {
	var config = require('./config')
} else {
	var config = require('./config-exemple')
}
const mysql = require('promise-mysql');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())


/* routes */
const userRoutes = require('./routes/userRoute')
const authRoutes = require('./routes/authRoutes')


const host = process.env.HOST_DB || config.db.host;
const database = process.env.DATABASE_DB || config.db.database;
const user = process.env.USER_DB || config.db.user;
const password = process.env.PASSWORD_DB || config.db.password;
mysql.createConnection({
	host: host,
	database: database,
	user: user,
	password: password,
}).then((db) => {
    console.log('connecté bdd');
	setInterval(async function () {
		let res = await db.query('SELECT 1');
	}, 10000);
	app.get('/', (req, res, next)=>{
		res.json({msg: 'connexion établie!', status: 200})
    })
    

/* db routes */

userRoutes(app, db);
authRoutes(app,db);


})
const PORT =  process.env.PORT || 8000;
app.listen(PORT, ()=>{
	console.log('listening port: '+PORT)+'all is ok';
})